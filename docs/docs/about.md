## Background

<p style="text-align:justify;"> Detention model will be useful to the port authorities by alerting them to an incoming vessel which has a higher or lower risk of getting detained. 
Armed with this information they can take appropriate actions on whether to allow the port to enter the terminal or 
dock it somewhere so that better vessels can be given a higher priority. </p> 
<p style="text-align:justify;">The reason is that a vessel with higher detention risk may take up a lot of time at the terminal, thereby, blocking other vessels and potentially causing supply chain bottlenecks.
On the other hand, port authorities can use this information to scrutinize the vessels more when they arrive at the terminal. 
From RightShip's point of view, this feature could be part of Maritime Emission Portal (MEP) to equip the ports with better decision-making capabilities. </p>